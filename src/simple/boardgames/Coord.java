package simple.boardgames;

import java.util.Arrays;
import java.util.StringJoiner;

public class Coord {
    private int[] _entries;
    
    public int length() { return _entries.length; }
    public int x() { return entry(0); }
    public int y() { return entry(1); }
    public int z() { return entry(2); }
    public int entry(int idx) {
        if (length() <= idx || idx < 0) {
            throw new RuntimeException(String.format("Coord %s: Out of bounds request for index [%d]", this.toString(), idx));
        }
        return _entries[idx];
    }
    
    public int[] entries() { return _entries; }
    
    public Coord(int... entries) {
        _entries = entries;
    }
    public Coord(Coord other) {
        _entries = Arrays.copyOf(other.entries(), other.length());
    }
    
    public Coord copy() {
        return new Coord(this);
    }
    
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Coord)) {
           return false;
        }
        Coord otherTuple = (Coord)other;
        if (this._entries.length != otherTuple._entries.length) {
            return false;
        }
        for (int i=0; i<this._entries.length; i++) {
            if (this._entries[i] != otherTuple._entries[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode(){
        return Arrays.hashCode(_entries);
    }
    
    @Override
    public String toString() {
        StringJoiner str = new StringJoiner(", ", "(", ")");        
        for (int entry : _entries) {
          str.add(""+entry);
        }
        
        return str.toString();
    }
}
